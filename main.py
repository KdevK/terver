import argparse

from flask import Flask, render_template

import pandas as pd
import csv
import numpy as np
import scipy.stats as st
import math
import matplotlib.pyplot as plt

app = Flask(__name__)


def start_calculations(dataset, rel):
    result = dict()

    dataset_len_n = len(dataset)
    result['n'] = dataset_len_n

    xmin = min(dataset)
    result['xmin'] = xmin

    xmax = max(dataset)
    result['xmax'] = xmax

    intervals_cnt_k = round(1 + 3.322 * math.log(dataset_len_n, 10))
    result['k'] = intervals_cnt_k

    interval_range_l = round((xmax - xmin) / intervals_cnt_k, 1)
    result['l'] = interval_range_l

    intervals = dict()
    interval_start = xmin - 0.1
    interval_end = interval_start + interval_range_l
    for index in range(intervals_cnt_k):
        intervals[index + 1] = [interval_start, interval_end]
        interval_start = round(interval_end, 1)
        interval_end = round(interval_end + interval_range_l, 1)
    result["intervals"] = intervals

    Ni = dict()
    for number in dataset:
        for index, value in intervals.items():
            if value[0] <= number < value[1]:
                Ni[index] = Ni.get(index, 0) + 1
                break
    result['numbers_cnt_in_interval'] = Ni

    XmidI = dict()
    XmidI[1] = round((intervals[1][0] + intervals[1][1]) / 2, 2)
    for i in range(1, intervals_cnt_k):
        XmidI[i + 1] = round(XmidI[1] + i * interval_range_l, 2)
    result['x_mid_in_each_interval'] = XmidI

    x_mid_chosen = 0.0
    for i in range(intervals_cnt_k):
        x_mid_chosen += Ni[i + 1] * XmidI[i + 1]
    x_mid_chosen = round(x_mid_chosen / dataset_len_n, 2)
    result['x_mid_chosen'] = x_mid_chosen

    Zi = dict()
    for i in range(intervals_cnt_k):
        Zi[i + 1] = round(XmidI[i + 1] - x_mid_chosen, 2)
    result['zi'] = Zi

    sample_variance = 0
    for i in range(intervals_cnt_k):
        sample_variance += (Zi[i + 1] ** 2) * Ni[i + 1]
    sample_variance = round(sample_variance / dataset_len_n, 2)
    result['sample_variance'] = sample_variance

    sample_mean = round(sample_variance ** 0.5, 2)
    result['sample_mean'] = sample_mean

    relative_frequency_i = dict()
    for i in range(intervals_cnt_k):
        relative_frequency_i[i + 1] = round(Ni[i + 1] / dataset_len_n, 2)
    result['relative_frequency_i'] = relative_frequency_i

    NiAccumulated = dict()
    NiAccumulated[1] = Ni[1]
    for i in range(2, intervals_cnt_k + 1):
        NiAccumulated[i] = Ni[i] + NiAccumulated[i - 1]
    result['numbers_cnt_in_interval_accumulated'] = NiAccumulated

    median_interval = 0
    for i in range(intervals_cnt_k):
        if dataset_len_n // 2 + 1 < NiAccumulated[i + 1]:
            median_interval = i + 1
            break

    median = round(intervals[median_interval][0] + (0.5 * dataset_len_n - NiAccumulated[median_interval])
                   * interval_range_l / Ni[median_interval], 2)
    result['median'] = median

    modal_interval = max(relative_frequency_i.values())
    modal_index = list(relative_frequency_i.values()).index(modal_interval) + 1

    modal = round((intervals[modal_index][0] + ((Ni[modal_index] - Ni[modal_index - 1]) /
                                                ((Ni[modal_index] - Ni[modal_index - 1]) + (
                                                        Ni[modal_index] - Ni[modal_index - 1])) *
                                                interval_range_l)), 2)
    result['modal'] = modal

    asymmetry_factor = 0
    kurtosis_factor = 0
    for i in range(intervals_cnt_k):
        a = (Zi[i + 1] ** 3) * relative_frequency_i[i + 1]
        asymmetry_factor += a
        kurtosis_factor += a * Zi[i + 1]
    sample_mean_power = sample_mean ** 3
    asymmetry_factor = round(asymmetry_factor / sample_mean_power, 2)
    kurtosis_factor = round(kurtosis_factor / (sample_mean_power * sample_mean) - 3, 2)
    result['asymmetry_factor'] = asymmetry_factor
    result['kurtosis_factor'] = kurtosis_factor

    variation_coefficient = round(sample_mean / x_mid_chosen * 100, 2)
    result['variation_coefficient'] = variation_coefficient

    t_crit = np.abs(st.t.ppf((1 - rel) / 2, dataset_len_n - 1))
    b = t_crit * sample_mean / (dataset_len_n ** 0.5)
    confidence_interval_general_mean = (round(x_mid_chosen - b, 2), round(x_mid_chosen + b, 2))
    result['confidence_interval_general_mean'] = confidence_interval_general_mean

    left_chi2 = st.chi2.ppf((1 + rel) / 2, df=dataset_len_n - 1)
    right_chi2 = st.chi2.ppf((1 - rel) / 2, df=dataset_len_n - 1)
    confidence_interval_general_variance = (round((dataset_len_n - 1) * sample_variance / left_chi2, 2), round(
        (dataset_len_n - 1) * sample_variance / right_chi2, 2))
    result['confidence_interval_general_variance'] = confidence_interval_general_variance

    return result


def draw(context):

    x = []
    y = []

    for i in range(1, context['k']+1):
        x.append(context['intervals'][i][0])
        y.append(context['relative_frequency_i'][i])

    plt.bar(x, y, width=context['l']*0.95, color='C0')
    axes = plt.gca()
    axes.yaxis.grid(color='white')
    plt.savefig('static/plot.png')


@app.route('/')
def hello():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', help='path to xls file with dataset')
    parser.add_argument('--column', default=1, help='column to calculate')
    parser.add_argument('--rel', default=0.95, help='reliability')

    args = vars(parser.parse_args())

    ds = pd.read_excel(args['path'], skiprows=1, usecols=[int(args['column'])], engine='openpyxl')
    ds.to_csv('input.csv')

    dataset = []
    with open('input.csv', 'r', newline='') as csv_read:
        reader = csv.reader(csv_read, delimiter=',')
        for row in reader:
            dataset.append(float(row[1]))
    print(start_calculations(dataset, float(args['rel'])))
    answer = start_calculations(dataset, float(args['rel']))
    draw(context=answer)
    return render_template('main.html', context=answer)

app.run()